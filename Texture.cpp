#include "Texture.h"


Texture::Texture(const std::string& file, GLint param){

    //For loading and converting
    QImage img, glformat;

    //If file could not be loaded
    if(!img.load(QString::fromStdString(file))){
        perror("The file could not be loaded!");
        perror("Please make sure the file path is correct, or not misspelled.");
    }

    //Convert to accepted GL format
    glformat = QGLWidget::convertToGLFormat(img);

    //Generate name/id
    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);

    //Apply texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, glformat.width(), glformat.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, glformat.bits());


    //Wrap modes, min-mag filter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, param);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, param);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

}

//Destructor(???) test
Texture::~Texture(){
    glDeleteTextures(1, &texID);
}

unsigned int Texture::getWidth(){
    return width;
}

unsigned int Texture::getHeight(){
    return height;
}

void Texture::setActiveTex(){
    glBindTexture(GL_TEXTURE_2D, texID);
}
