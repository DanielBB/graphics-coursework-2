#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent){

    /*
    Supplying the this pointer essentially passes the
    MainWindow's hidden this pointer to the class object and 
    defines it as the widgets parent 
    */
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);


    //The windows main widget
    darthWidget = new DarthWidget(this);
    windowLayout->addWidget(darthWidget);

    yax = new QLabel("Saber Rotation Speed", this);
    yax->setMaximumHeight(10);
    windowLayout->addWidget(yax, 0, Qt::AlignHCenter);

    //create the slider and add it to the widget
    slider = new QSlider(Qt::Horizontal);
    windowLayout->addWidget(slider);

    armLabel = new QLabel("Move Grievous arms", this);
    armLabel->setMaximumHeight(10);
    windowLayout->addWidget(armLabel, 0, Qt::AlignHCenter);

    armSlider = new QSlider(Qt::Horizontal);
    windowLayout->addWidget(armSlider);

    timer = new QTimer(this);

    //Sliders
    connect(slider, SIGNAL(valueChanged(int)), darthWidget, SLOT(speedSlider(int)));
    connect(armSlider, SIGNAL(valueChanged(int)), darthWidget, SLOT(rotateArms(int)));

    //Timer
    connect(timer, SIGNAL(timeout()), darthWidget, SLOT(rotateMarc()));
    connect(timer, SIGNAL(timeout()), darthWidget, SLOT(rotateEarth()));
    connect(timer, SIGNAL(timeout()), darthWidget, SLOT(increaseSpeed()));
    connect(timer, SIGNAL(timeout()), darthWidget, SLOT(rotateGrievous()));
    timer->start();

}

MainWindow::~MainWindow(){

}

void MainWindow::ResetInterface(){

    darthWidget->update();
    update();
}

