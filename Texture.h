#pragma once

#include <string>
#include <QGLWidget>
#include <QImage>
#include <GL/glu.h>
#include <iostream>
#include <cstdio>

#include "Texture.h"

/*
A class capable of handling texture draw calls
*/
class Texture{

public:

    Texture(const std::string& file, GLint param);
 
    ~Texture();

    void setActiveTex();

    unsigned int getWidth();
    unsigned int getHeight();

private:

    // Texture(&Texture);
    unsigned int width;
    unsigned int height;

    GLuint texID;
};