#pragma once

#include <iostream>
#include <QGLWidget>
#include <GL/glu.h>
#include "DarthWidget.h"
#include "Texture.h"


typedef struct material{

    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat emission;

} material;


class DarthWidget : public QGLWidget{

    Q_OBJECT

public: 

    DarthWidget(QWidget *parent);
    ~DarthWidget();

public slots: 

    void increaseSpeed();
    void speedSlider(int boost);
    void rotateArms(int ar);
    void rotateGrievous();
    void rotateMarc();
    void rotateEarth();



protected:
    
    //Standard OpenGL functions
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    //Generate a glu object
    void makeCylinder(GLUquadricObj *quad, GLdouble base,  GLdouble top, GLdouble height,  GLint slices, GLint stacks);
    void makeSphere(GLUquadric *quad, GLdouble radius, GLint slices , GLint stacks);
    void drawSkybox();
    void drawGrievous();

    void printGLError(GLenum code);

    void setupMaterial(material &mat);

private:

    Texture *floor;
    Texture *marc;
    Texture *earthTex;
    Texture *space;
    Texture *redSaber;
    Texture *greenSaber;
    Texture *grievousHead;
    Texture *blueSaber;


    int angle;
    int grievousAngle;
    int lJointAngle;
    int rJointAngle;
    int saberSpeed;
    int armAngle;
    int ar;

    int marcX;
    int marcY;

    int earthX;
    int earthY;

    //General Grievous features test
    GLUquadricObj* gHead; 
    GLUquadricObj* gBody; 

    GLUquadricObj* grJoint;
    GLUquadricObj* glJoint;

    GLUquadricObj* gr2Joint;
    GLUquadricObj* gl2Joint;

    GLUquadricObj* grArm;
    GLUquadricObj* glArm;
    
    GLUquadricObj* grElbow;
    GLUquadricObj* glElbow;

    GLUquadricObj* grForearm;
    GLUquadricObj* glForearm;

    GLUquadricObj* grHand;
    GLUquadricObj* glHand;

    GLUquadricObj* grThigh;
    GLUquadricObj* glThigh;

    GLUquadricObj* grKnee;
    GLUquadricObj* glKnee;

    GLUquadricObj* grCalf;
    GLUquadricObj* glCalf;

    GLUquadricObj* redLightSaber;
    GLUquadricObj* greenLightSaber;

    //Middle arms
    GLUquadricObj* glmidJoint;
    GLUquadricObj* grmidJoint;
    GLUquadricObj* glmidArm;
    GLUquadricObj* grmidArm;
    GLUquadricObj* glmidElbow;
    GLUquadricObj* grmidElbow;
    GLUquadricObj* glmidForearm;
    GLUquadricObj* grmidForearm;
    GLUquadricObj* glmidHand;
    GLUquadricObj* grmidHand;
    GLUquadricObj* glmidSaber;
    GLUquadricObj* grmidSaber;



    //Earth and Moon
    GLUquadricObj* earth;
    GLUquadricObj* marcMoon;
};