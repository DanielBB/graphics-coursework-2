#pragma once

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <iostream>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QLabel>
#include <QPushButton>

#include "DarthWidget.h"
//A Main Window class
class MainWindow : public QWidget{

public:
    
    MainWindow(QWidget *parent);
    ~MainWindow();

    void ResetInterface();

private:
    
    QMenuBar *menuBar;
    QMenu *fileMenu;
    QAction *quit;
    QLabel *yax;
    QLabel *armLabel;

    // The type of window layout
    QBoxLayout *windowLayout;

    //The central widget, where we render OpenGL features
    DarthWidget *darthWidget;

    //Slider and timer objects
    QSlider *slider;
    QSlider *armSlider;
    QTimer *timer;
    QPushButton *button;



};
