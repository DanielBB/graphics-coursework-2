
#include <QApplication>
#include "MainWindow.h"

int main(int argc, char *argv[]){

    //Set up the QApp
    QApplication app(argc, argv);

    //Create a main window instance
    MainWindow *window = new MainWindow(NULL);

    window->resize(700, 700);

    window->show();

    app.exec();

    delete window;

    return 0;
}