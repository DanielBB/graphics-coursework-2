#include "DarthWidget.h"

/*
Two material structs.
The below function setupMaterial(), converts
between them later on. We do this by extracting the 
values for each type of light, ambient, diffuse, specular and 
the emmissive quantity.
*/
static material whiteShiny{

  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0

};

static material blackPlastic{

    { 0.0, 0.0, 0.0, 1.0},
    { 0.01, 0.01, 0.01, 1.0},
    { 0.5, 0.5, 0.5, 1.0},
    100.0
};

//Handles conversion between different materials
void DarthWidget::setupMaterial(material &mat){
  
  material *params = &mat;
  glMaterialfv(GL_FRONT, GL_AMBIENT,    params->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    params->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   params->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   params->emission);
}

/*
The constructor uses an initialization list to set up default values for certain variables
which are used the signal and slot system of the program.
*/
DarthWidget::DarthWidget(QWidget *parent) : QGLWidget(parent), angle(0), grievousAngle(90), 
                        lJointAngle(0), rJointAngle(30), saberSpeed(5), marcY(0), earthY(0), ar(0){

}

/*
Initialize the main components we will be using
in the program.
*/
void DarthWidget::initializeGL(){

    /*Background color*/
    glClearColor(0.3, 0.3, 0.3, 0.0);

    /*Enable depth testing and texturing*/
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);


    GLfloat light_pos[] = {0., 0., 1., 0.};

    //Enable lighting calculations 
    glEnable(GL_LIGHT0);   
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);

    /*Set up the textures that will be used in teh program*/
    floor = new Texture("tex/metal_floor.jpg", GL_CLAMP);
    marc = new Texture("tex/Moi.ppm", GL_CLAMP);
    earthTex = new Texture("tex/earth.ppm", GL_CLAMP);
    space = new Texture("tex/space.png", GL_REPEAT);
    redSaber = new Texture("tex/red_saber.jpg", GL_CLAMP);
    greenSaber = new Texture("tex/green_saber.jpg", GL_CLAMP);
    grievousHead = new Texture("tex/grievous.png", GL_CLAMP);
    blueSaber = new Texture("tex/blue_saber.jpg", GL_CLAMP);

    //General Grievous models
    gHead = gluNewQuadric();
    gBody = gluNewQuadric();

    grJoint= gluNewQuadric();
    glJoint = gluNewQuadric();

    gr2Joint = gluNewQuadric();
    gl2Joint = gluNewQuadric();

    grArm = gluNewQuadric();
    glArm = gluNewQuadric();
    
    grElbow = gluNewQuadric();
    glElbow = gluNewQuadric();

    grForearm = gluNewQuadric();
    glForearm = gluNewQuadric();

    grHand = gluNewQuadric();
    glHand = gluNewQuadric();

    grThigh = gluNewQuadric();
    glThigh = gluNewQuadric();

    grKnee = gluNewQuadric();
    glKnee = gluNewQuadric();

    grCalf = gluNewQuadric();
    glCalf = gluNewQuadric();

    redLightSaber = gluNewQuadric();
    greenLightSaber = gluNewQuadric();

    glmidJoint = gluNewQuadric();
    grmidJoint = gluNewQuadric() ;
    glmidArm = gluNewQuadric();
    grmidArm = gluNewQuadric();
    glmidElbow = gluNewQuadric();
    grmidElbow = gluNewQuadric();
    glmidForearm = gluNewQuadric();
    grmidForearm = gluNewQuadric();
    glmidHand = gluNewQuadric();
    grmidHand = gluNewQuadric();
    glmidSaber = gluNewQuadric();
    grmidSaber = gluNewQuadric();

    // Earth and moon models
    earth = gluNewQuadric();
    marcMoon = gluNewQuadric();
    
}

/*Define how OpenGL will react to resizing*/
void DarthWidget::resizeGL(int w, int h){

    glViewport(0, 0, w, h);
    glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    glOrtho(-11.0, 11.0, -11.0, 11.0, 0.1, 500.0);
}

/*Draw the scene*/
void DarthWidget::paintGL(){
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    /*Set up the "camera"*/
    gluLookAt(8.,8.,8., 0.0,0.0,0.0, 0.0,1.0,0.0);
    
    /*The Skybox within which we build the scene*/
    space->setActiveTex();
    drawSkybox();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    
    /*Draw the central platform*/
    glColor3f(1.0, 1.0, 1.0);
    glPushMatrix();
        floor->setActiveTex();
        glNormal3f(0.0, 1.0, 0.0);
        glBegin(GL_POLYGON);
            glTexCoord2f(0.0, 0.0);
            glVertex3f(11.0, 0.0, 11.0);
            glTexCoord2f(1.0, 0.0);
            glVertex3f(11.0, 0.0, -11.0);
            glTexCoord2f(1.0, 1.0);
            glVertex3f(-11.0,  0.0, -11.0);
            glTexCoord2f(0.0, 1.0);
            glVertex3f(-11.0,  0.0, 11.0);
        glEnd();
    glPopMatrix();

    //Earth and MarcMoon
    glPushMatrix();
    
        earthTex->setActiveTex();
        glRotatef(earthY*0.5, 0.0, 1.0, 0.0);
        glTranslatef(10.0, 7.0, 0.0);
            glPushMatrix();
                glRotatef(0.5*earthY, 0.0, 1.0, 0.0);
                gluQuadricTexture(earth,1);
                makeSphere(earth, 1.0, 20, 20);

                glPushMatrix();

                    marc->setActiveTex();
                    glRotatef(0.5*marcY, 0.0, 1.0, 0.0);
                    glTranslatef(2.0, 0.0, 0.0);

                    gluQuadricTexture(marcMoon,1);
                    makeSphere(marcMoon, 0.3, 10, 10);
                glPopMatrix();
    glPopMatrix();
        glPopMatrix();
        drawGrievous();
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0, 1.0, 1.0);


  
    glFlush();

    //Error checking
    GLenum error = GL_NO_ERROR;
    while((error = glGetError()) != GL_NO_ERROR){
        
        printGLError(error);

    }


}

/*
Code for models
*/
void DarthWidget::makeSphere(GLUquadricObj *quad, GLdouble radius, GLint slices, GLint stacks){
    gluSphere(quad, radius, slices, stacks);
}

void DarthWidget::makeCylinder(GLUquadricObj *quad, GLdouble base,  GLdouble top, GLdouble height,  GLint slices, GLint stacks){
    gluCylinder(quad, base, top, height, slices, stacks);
}

/*
Code for skybox
*/
void DarthWidget::drawSkybox(){

    glDepthMask(GL_FALSE);
    glBegin(GL_POLYGON);

        //Bottom
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-100.0 ,-100.0 , 100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(100.0, -100.0, 100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(100.0, -100.0, -100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(-100.0, -100.0 , -100.0);

        //Left
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-100.0 ,-100.0 , 100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(100.0, -100.0, 100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(100.0, 100.0, 100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(-100.0, 100.0 , 100.0);

        //Front
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-100.0 ,-100.0 , 100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(-100.0, -100.0, -100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(-100.0, 100.0, -100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(-100.0, 100.0 , 100.0);

        //Right
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-100.0 ,-100.0 , -100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(100.0, -100.0, -100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(100.0, 100.0, -100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(-100.0, 100.0 , -100.0);

        //Back
        glTexCoord2f(0.0, 0.0);
        glVertex3f(100.0 ,-100.0 , 100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(100.0, -100.0, -100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(100.0, 100.0, -100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(100.0, 100.0 , 100.0);

        //Top
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-100.0 ,-10.0 , 100.0 );
        glTexCoord2f(5.0, 0.0);
        glVertex3f(100.0, -10.0, 100.0 );
        glTexCoord2f(5.0, 5.0);
        glVertex3f(100.0, -10.0, -100.0);
        glTexCoord2f(0.0, 5.0);
        glVertex3f(-100.0, -10.0 , -100.0);
    glEnd();

    glDepthMask(GL_TRUE);
}

/*
General Grievous
*/
void DarthWidget::drawGrievous(){

    glPushMatrix();
    glScalef(0.5, 0.5, 0.5);
    glRotatef((0.5 * grievousAngle), 0.0, 1.0, 0.0);
    glTranslatef(0.0, 0.0, 13.0);
    glTranslatef(0.0, 4.0, 0.0);
    glRotatef(-90.0, 1.0, 0.0, 0.0);
    makeCylinder(gBody, 1.0, 1.0, 5.0, 20, 20);
    glRotatef(90, 1.0, 0.0 ,0.0);
        glPushMatrix();
            setupMaterial(blackPlastic);
            grievousHead->setActiveTex();
            gluQuadricTexture(gHead,1);
            glTranslatef(0.0, 5.5, 0.0);
            glRotatef(-90.0, 0.0, 1.0, 0.0);
            glRotatef(-90.0, 1.0, 0.0, 0.0);
            makeSphere(gHead, 1.0, 10, 10);
            setupMaterial(whiteShiny);
        glPopMatrix();
        glPushMatrix();
        glDisable(GL_TEXTURE_2D);
            glTranslatef(0.0, 4.0, 1.4);
            setupMaterial(blackPlastic);
            makeSphere(grJoint, 0.4, 10, 10);
            setupMaterial(whiteShiny);
            glPushMatrix();
                glTranslatef(0.0, 0.0, 0.2);
                makeCylinder(grArm, 0.3, 0.3, 2.5, 20, 20);
                    glPushMatrix();
                        glTranslatef(0.0, 0.0, 2.5);
                        glRotatef(-armAngle, 0.0, 1.0, 0.0);
                        makeSphere(grElbow, 0.3, 20, 20);
                            glPushMatrix();
                                glTranslatef(0.0, 0.0, 0.2);
                                glRotatef(70.0, 0.0, 1.0, 0.0);
                                makeCylinder(grForearm, 0.3, 0.3, 2.5, 20, 20);
                                    glPushMatrix();
                                        glColor3f(0.0, 0.0, 0.0);
                                        glTranslatef(0.0, 0.0, 2.5);
                                        glRotatef(-angle, 0.0, 0.0, 1.0);
                                        makeSphere(grHand, 0.3, 20, 20);
                                        glColor3f(1.0, 1.0, 1.0);
                                            glPushMatrix();
                                                glEnable(GL_TEXTURE_2D);
                                                redSaber->setActiveTex();
                                                gluQuadricTexture(redLightSaber,1);
                                                glRotatef(90, 1.0, 0.0, 0.0);                                                
                                                makeCylinder(redLightSaber, 0.1, 0.1, 6.0, 20, 20);
                                                glDisable(GL_TEXTURE_2D);
                                            glPopMatrix();
                                    glPopMatrix();
                            glPopMatrix();
                    glPopMatrix();
            glPopMatrix();
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0.0, 4.0, -1.4);
            makeSphere(glJoint, 0.4, 10, 10);
            glPushMatrix();
                glRotatef(180, 1.0, 0.0, 0.0);
                glTranslatef(0.0, 0.0, 0.2);
                makeCylinder(glArm, 0.3, 0.3, 2.5, 20, 20);
                    glPushMatrix();
                    glRotatef(180, 1.0, 0.0, 0.0);
                        glTranslatef(0.0, 0.0, -2.5);
                        glRotatef(armAngle, 0.0, 1.0, 0.0);
                        makeSphere(glElbow, 0.3, 20, 20);
                            glPushMatrix();
                                glRotatef(180, 1.0, 0.0, 0.0);
                                glTranslatef(0.0, 0.0, -0.2);
                                glRotatef(70.0, 0.0, 1.0, 0.0);
                                makeCylinder(glForearm, 0.3, 0.3, 2.5, 20, 20);
                                    glPushMatrix();
                                        glRotatef(180, 1.0, 0.0, 0.0);
                                        glTranslatef(0.0, 0.0, -2.5);
                                        makeSphere(glHand, 0.3, 20, 20);
                                            glPushMatrix();
                                                glEnable(GL_TEXTURE_2D);
                                                greenSaber->setActiveTex();
                                                gluQuadricTexture(greenLightSaber,1);
                                                glRotatef(180, 1.0, 0.0, 0.0);
                                                glRotatef(-90, 1.0, 0.0, 0.0);
                                                glRotatef(-angle, 0.0, 1.0, 0.0);
                                                makeCylinder(greenLightSaber, 0.1, 0.1, 6.0, 20, 20);
                                                glDisable(GL_TEXTURE_2D);
                                            glPopMatrix();
                                    glPopMatrix();
                            glPopMatrix();
                    glPopMatrix();
            glPopMatrix();
        glPopMatrix();
        glPushMatrix();
        glDisable(GL_TEXTURE_2D);
            glTranslatef(0.0, 2.0, 1.4);
            makeSphere(grmidJoint, 0.4, 10, 10);
            glPushMatrix();
                glTranslatef(0.0, 0.0, 0.2);
                makeCylinder(grmidArm, 0.3, 0.3, 2.5, 20, 20);
                    glPushMatrix();
                        glTranslatef(0.0, 0.0, 2.5);
                        makeSphere(grmidElbow, 0.3, 20, 20);
                            glPushMatrix();
                                glTranslatef(0.0, 0.0, 0.2);
                                glRotatef(70.0, 0.0, 1.0, 0.0);
                                makeCylinder(grmidForearm, 0.3, 0.3, 2.5, 20, 20);
                                    glPushMatrix();
                                        glColor3f(0.0, 0.0, 0.0);
                                        glTranslatef(0.0, 0.0, 2.5);
                                        glRotatef(40, 1.0, 0.0, 0.0);
                                        makeSphere(grmidHand, 0.3, 20, 20);
                                        glColor3f(1.0, 1.0, 1.0);
                                            glPushMatrix();
                                                glEnable(GL_TEXTURE_2D);
                                                blueSaber->setActiveTex();
                                                gluQuadricTexture(grmidSaber,1);
                                                glRotatef(90, 1.0, 0.0, 0.0);                                                
                                                makeCylinder(grmidSaber, 0.1, 0.1, 6.0, 20, 20);
                                                glDisable(GL_TEXTURE_2D);
                                            glPopMatrix();
                                    glPopMatrix();
                            glPopMatrix();
                    glPopMatrix();
            glPopMatrix();
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0.0, 2.0, -1.4);
                makeSphere(glmidJoint, 0.4, 10, 10);
                    glPushMatrix();
                    glRotatef(180, 1.0, 0.0, 0.0);
                    glTranslatef(0.0, 0.0, 0.2);
                    makeCylinder(glmidArm, 0.3, 0.3, 2.5, 20, 20);
                        glPushMatrix();
                            glRotatef(180, 1.0, 0.0, 0.0);
                            glTranslatef(0.0, 0.0, -2.5);
                            makeSphere(glmidElbow, 0.3, 20, 20);
                            glPushMatrix();
                                glRotatef(180, 1.0, 0.0, 0.0);
                                glTranslatef(0.0, 0.0, -0.2);
                                glRotatef(70.0, 0.0, 1.0, 0.0);
                                makeCylinder(glmidForearm, 0.3, 0.3, 2.5, 20, 20);
                                    glPushMatrix();
                                        glRotatef(180, 1.0, 0.0, 0.0);
                                        glTranslatef(0.0, 0.0, -2.5);
                                        makeSphere(glmidHand, 0.3, 20, 20);
                                            glPushMatrix();
                                                glEnable(GL_TEXTURE_2D);
                                                    blueSaber->setActiveTex();
                                                    gluQuadricTexture(glmidSaber,1);
                                                    glRotatef(180, 1.0, 0.0, 0.0);
                                                    glRotatef(-90, 1.0, 0.0, 0.0);
                                                    makeCylinder(glmidSaber, 0.1, 0.1, 6.0, 20, 20);
                                                glDisable(GL_TEXTURE_2D);
                                            glPopMatrix();
                                    glPopMatrix();
                            glPopMatrix();
                    glPopMatrix();
            glPopMatrix();
        glPopMatrix();
                    
        glPushMatrix();
            glTranslatef(0.0, 0.0, 1.0);
            glRotatef(30.0, 0.0, 0.0, 1.0);
            makeSphere(gr2Joint, 0.4, 10, 10);
                glPushMatrix();
                    glRotatef(90, 1.0, 0.0, 0.0);
                    makeCylinder(grThigh, 0.3, 0.3, 2.5, 20, 20);
                        glPushMatrix();
                            glTranslatef(0.0, 0.0, 2.5);
                            makeSphere(grKnee, 0.4, 10, 10);
                                glPushMatrix();
                                    glTranslatef(0.0, 0.0, 0.3);
                                    glRotatef(-30.0, 0.0, 1.0, 0.0);         
                                    makeCylinder(grCalf, 0.3, 0.3, 2.5, 20, 20);
                                glPopMatrix();
                        glPopMatrix();
                glPopMatrix();
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0.0, 0.0, -1.0);
            makeSphere(gl2Joint, 0.4, 10, 10);
                glPushMatrix();
                    glRotatef(180.0, 1.0, 0.0, 0.0);
                    glRotatef(-90, 1.0, 0.0, 0.0);
                    makeCylinder(glThigh, 0.3, 0.3, 2.5, 20, 20);
                        glPushMatrix();
                            glTranslatef(0.0, 0.0, 2.5);
                            makeSphere(glKnee, 0.4, 10, 10);
                                glPushMatrix();
                                    glRotatef(-30.0, 0.0, 1.0, 0.0);
                                    makeCylinder(glCalf, 0.3, 0.3, 2.5, 20, 20);
                                glPopMatrix();
                        glPopMatrix();
                glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

/*
User interface functions. 
Sliders and Timers.
*/
void DarthWidget::increaseSpeed(){

    angle += 0.5 * saberSpeed;
    this->update();
}

void DarthWidget::rotateArms(int ar){
    armAngle = ar;
    this->update();
}

void DarthWidget::speedSlider(int boost){

    saberSpeed = boost;
    this->update();
}

void DarthWidget::rotateGrievous(){
    grievousAngle += 5;
}

void DarthWidget::rotateMarc(){
    marcY += 5;
    this->update();
}

void DarthWidget::rotateEarth(){
    earthY += 3;
    this->update();
}

/*
Function that returns an error message based on
the GLenum value returned from glGetError().
Basically OpenGL error checking.
*/
void DarthWidget::printGLError(GLenum code){

    switch(code){

        case GL_INVALID_ENUM:
        std::cout << "GL_INVALID_ENUM" << std::endl;
        break;

        case GL_INVALID_VALUE:
        std::cout << "GL_INVALID_VALUE" << std::endl;
        break;

        case GL_INVALID_OPERATION:
        std::cout << "GL_INVALID_OPERATION" << std::endl;
        break;

        case GL_STACK_OVERFLOW:
        std::cout << "GL_STACK_OVERFLOW" << std::endl;
        break;

        case GL_STACK_UNDERFLOW:
        std::cout << "GL_STACK_UNDERFLOW" << std::endl;
        break;

        case GL_OUT_OF_MEMORY:
        std::cout << "GL_OUT_OF_MEMORY" << std::endl;
        break;

        case GL_INVALID_FRAMEBUFFER_OPERATION:
        std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;
        break;

        case GL_TABLE_TOO_LARGE:
        std::cout << "GL_TABLE_TOO_LARGE" << std::endl;
        break;

        default: 
        std::cout << "error" << std::endl;
    }
}

DarthWidget::~DarthWidget(){

    //Free memory used for Grievous
    gluDeleteQuadric(gBody);

    // ...and spheres
    gluDeleteQuadric(earth);
    gluDeleteQuadric(marcMoon);

    
}